package classannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Damian Terlecki
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
public @interface Author {
  String name() default "Damian Terlecki";
  String date();
}
