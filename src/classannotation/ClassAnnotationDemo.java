package classannotation;

import java.io.IOException;
import java.util.List;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.tree.AnnotationNode;
import jdk.internal.org.objectweb.asm.tree.ClassNode;
import jdk.internal.org.objectweb.asm.tree.MethodNode;

/**
 *
 * @author Damian Terlecki
 */
@Author(name = "Damian Terlecki", date = "18-12-2015")
public class ClassAnnotationDemo {

    @BugReport(severity = 1, assignedTo = "Damian Terlecki")
    public static void main(String[] args) throws IOException {
        ClassNode classNode = new ClassNode();

        ClassReader classReader = new ClassReader(
                ClassLoader.getSystemResourceAsStream(
                        "classannotation/ClassAnnotationDemo.class"));
        classReader.accept(classNode, 0);
        System.out.println("Class name : " + classNode.name);
        printAnnotation(classNode.invisibleAnnotations);
        List<MethodNode> methodNodes = classNode.methods;
        printMethodsAnnotations(methodNodes);
    }

    private static void printMethodsAnnotations(List<MethodNode> methodNodes) {
        methodNodes.stream().forEach((methodNode) -> {
            if (methodNode.invisibleAnnotations != null
                    && !methodNode.invisibleAnnotations.isEmpty()) {
                System.out.println("Method name : " + methodNode.name);
                printAnnotation(methodNode.invisibleAnnotations);
            }
        });
    }

    private static void printAnnotation(List<AnnotationNode> annotationList) {
        annotationList.stream().forEach((anNode) -> {
            System.out.println("Annotation descriptor : " + anNode.desc);
            System.out.println("Annotation attribute pairs : " + anNode.values);
        });
    }

}
